package com.mercury.test;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class MercuryToursTest {

	public static void main(String[] args) throws InterruptedException {

System.setProperty("webdriver.chrome.driver", "C:\\Automation Testing\\Tools\\chromedriver.exe");
WebDriver driver=new ChromeDriver(); //Interview Question What is web driver?
                                     //What is driver and ChromeDriver?
//abc
Thread.sleep(8000);

driver.manage().window().maximize();
Thread.sleep(5000);
driver.get("http://newtours.demoaut.com/");
Thread.sleep(5000);
//WebElement username=driver.findElement(By.name("userName"));
WebElement username=driver.findElement(By.xpath("//input[@name='userName']"));
username.sendKeys("dasd");
//WebElement password=driver.findElement(By.name("password"));
WebElement password=driver.findElement(By.xpath("//input[@name='password']"));
password.sendKeys("dasd");
//WebElement signin=driver.findElement(By.name("login"));
WebElement signin=driver.findElement(By.xpath("//input[@name='login']"));
signin.click();
Thread.sleep(2000);//validet Login;
String expectedtitle ="Find a Flight: Mercury Tours:";
String actualtitle1=driver.getTitle();
if (expectedtitle.equals(actualtitle1))
{
  System.out.println("pass");
}
else
{ 
  System.out.println("fail");

}
	
//default round trip is selected

WebElement triptype=driver.findElement(By.xpath("//input[@value='roundtrip']"));
if (triptype.isSelected()==true)
{
	 System.out.println("default roundtrip Checkbox selected");

}	
  else
{
	System.out.println("default roundtrip Checkbox not selected");
}

//verify oneway radio button is not selected
   WebElement triptypee=driver.findElement(By.xpath("//input[@value='oneway']"));//Relative Xpath
    if (triptypee.isSelected()==true)
    {
    	 System.out.println("default oneway selected");
    
    }	
   else
    {
    	System.out.println("default oneway not selected");
    }
    Thread.sleep(3000);
 //using select class for multiple drop down
    Select oSelect =new Select(driver.findElement(By.xpath("//select[@name='passCount']")));
    oSelect.selectByIndex(2);
    Thread.sleep(2000);
    Select oSelect1 =new Select(driver.findElement(By.name("fromPort")));
    oSelect1.selectByIndex(3);
    Thread.sleep(2000);
    Select oSelect2 =new Select(driver.findElement(By.name("fromMonth")));
    oSelect2.selectByIndex(6);
    Thread.sleep(1000);
    Select oSelect3 =new Select(driver.findElement(By.name("fromDay")));
    oSelect3.selectByIndex(25);
    Thread.sleep(1000);
    Select oSelect4 =new Select(driver.findElement(By.name("toPort")));
    oSelect4.selectByIndex(5);
    Select oSelect5 =new Select(driver.findElement(By.name("toMonth")));
    oSelect5.selectByIndex(9);
    Thread.sleep(1000);
    Select oSelect6 =new Select(driver.findElement(By.name("toDay")));
    oSelect6.selectByIndex(23);
    Thread.sleep(2000);
    WebElement triptypee1=driver.findElement(By.xpath("//input[@value='Business']"));
    triptypee1.click();
    Thread.sleep(2000);
    Select oSelect7 =new Select(driver.findElement(By.name("airline")));
    oSelect7.selectByIndex(2);
    Thread.sleep(2000);
  WebElement continue1=driver.findElement(By.name("findFlights"));
  continue1.click();
  Thread.sleep(1000);
  
  //code for select flight page
  WebElement triptypee2=driver.findElement(By.xpath("//input[@value='Blue Skies Airlines$361$271$7:10']"));
  triptypee2.click();
  Thread.sleep(1000);
  WebElement triptypee3=driver.findElement(By.xpath("//input[@value='Pangea Airlines$632$282$16:37']"));
  triptypee3.click();
  Thread.sleep(1000);
  WebElement continue2=driver.findElement(By.name("reserveFlights"));
  continue2.click();//code for book a flight page
  
  //Code for book a flight page passengers name
  
  WebElement firstname=driver.findElement(By.xpath("//input[@name='passFirst0']"));
  firstname.sendKeys("ARITRA");
  WebElement lastname=driver.findElement(By.xpath("//input[@name='passLast0']"));
  lastname.sendKeys("MAITY");
  Select oSelect8 =new Select(driver.findElement(By.name("pass.0.meal")));
  oSelect8.selectByIndex(3);
  Thread.sleep(1000);
  WebElement firstname1=driver.findElement(By.xpath("//input[@name='passFirst1']"));
  firstname1.sendKeys("ADRISH");
  WebElement lastname1=driver.findElement(By.xpath("//input[@name='passLast1']"));
  lastname1.sendKeys("MAITY");
  Select oSelect9 =new Select(driver.findElement(By.name("pass.1.meal")));
  oSelect9.selectByIndex(3);
  Thread.sleep(1000);
  WebElement firstname2=driver.findElement(By.xpath("//input[@name='passFirst2']"));
  firstname2.sendKeys("ANJALI");
  WebElement lastname2=driver.findElement(By.xpath("//input[@name='passLast2']"));
  lastname2.sendKeys("MAITI");
  Select oSelect10 =new Select(driver.findElement(By.name("pass.2.meal")));//how i create x path about this line????????
  oSelect10.selectByIndex(3);
  Thread.sleep(2000); //code for book a flight page credit card details
  
  Select oSelect11 =new Select(driver.findElement(By.name("creditCard")));
  oSelect11.selectByIndex(2);
  WebElement creditcard=driver.findElement(By.xpath("//input[@name='creditnumber']"));
  creditcard.sendKeys("11452935236545");
  Select oSelect12 =new Select(driver.findElement(By.name("cc_exp_dt_mn")));
  oSelect12.selectByIndex(4);
  Select oSelect13 =new Select(driver.findElement(By.name("cc_exp_dt_yr")));
  oSelect13.selectByIndex(6);
  Thread.sleep(2000);// credit card name
   WebElement ccfirstname=driver.findElement(By.xpath("//input[@name='cc_frst_name']"));
  ccfirstname.sendKeys("ARITRA");
  WebElement ccmiddlename=driver.findElement(By.xpath("//input[@name='cc_mid_name']"));
  ccmiddlename.sendKeys("");
  WebElement cclastname=driver.findElement(By.xpath("//input[@name='cc_last_name']"));
  cclastname.sendKeys("MAITY");
  Thread.sleep(2000);                                                               //CODE FOR BEELING ADDRESS check box selected
  
   WebElement checkbox1=driver.findElement(By.xpath("//input[@name='ticketLess']")); 
  checkbox1.click();
  Thread.sleep(1000);
  WebElement bAddress=driver.findElement(By.xpath("//input[@name='billAddress1']"));
  bAddress.clear();
  WebElement bAddress1=driver.findElement(By.xpath("//input[@name='billAddress1']"));
  bAddress1.sendKeys("AB-8/30 A,DESHBANDHUNAGAR");
  WebElement bAddress2=driver.findElement(By.xpath("//input[@name='billAddress2']"));
  bAddress2.sendKeys("BAGUIATI");
  WebElement city=driver.findElement(By.xpath("//input[@name='billCity']"));
  city.clear();
  WebElement city1=driver.findElement(By.xpath("//input[@name='billCity']"));
  city1.sendKeys("KOLKATA");
  WebElement province=driver.findElement(By.xpath("//input[@name='billState']"));
  province.clear();
  WebElement province1=driver.findElement(By.xpath("//input[@name='billState']"));
  province1.sendKeys("WEST BENGAL");
  WebElement postal=driver.findElement(By.xpath("//input[@name='billZip']"));
  postal.clear();
  WebElement postal1=driver.findElement(By.xpath("//input[@name='billZip']"));
  postal1.sendKeys("700059");
  Select oSelect14 =new Select(driver.findElement(By.xpath("//select[@name='billCountry']")));
  oSelect14.selectByIndex(91);
  
  //CODE FOR DELIVERY ADDRESS//code for delivery address checkbox2 ?????
  
  WebElement delAddress=driver.findElement(By.xpath("//input[@name='delAddress1']"));
  delAddress.clear();
  WebElement delAddress1=driver.findElement(By.xpath("//input[@name='delAddress1']"));
  delAddress1.sendKeys("AB 8/30A,DESHBANDHUNAGAR");
  WebElement delAddress2=driver.findElement(By.xpath("//input[@name='delAddress2']"));
  delAddress2.sendKeys("BAGUIATI");
  WebElement delCity=driver.findElement(By.xpath("//input[@name='delCity']"));
  delCity.clear();
  WebElement delCity1=driver.findElement(By.xpath("//input[@name='delCity']"));
  delCity1.sendKeys("KOLKATA");
  WebElement delState=driver.findElement(By.xpath("//input[@name='delState']"));
  delState.clear();
  WebElement delState1=driver.findElement(By.xpath("//input[@name='delState']"));
  delState1.sendKeys("WEST BENGAL");
  WebElement delZip=driver.findElement(By.xpath("//input[@name='delZip']"));
  delZip.clear();
  WebElement delZip1=driver.findElement(By.xpath("//input[@name='delZip']"));
  delZip1.sendKeys("700059");
  Select oSelect15 =new Select(driver.findElement(By.xpath("//select[@name='delCountry']")));
  oSelect15.selectByIndex(91);//how to handel the alert
  Alert alert1=driver.switchTo().alert();
  alert1.accept();
  WebElement Purchase=driver.findElement(By.xpath("//input[@name='buyFlights']"));
  Purchase.click();
 // WebElement Print=driver.findElement(By.xpath("//img[@src='/images/printit.gif']"));
  //Print.click();
  WebElement Logout=driver.findElement(By.xpath("//img[@src='/images/forms/Logout.gif']"));
  Logout.click();
  
  
 //driver.close();

	}

}

